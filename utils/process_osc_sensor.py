import csv
import json
import sys
import argparse

# Script to read the csv file created by https://apps.automeris.io/wpd/ and convert into correct json. 
# Please remove the headers from the csv first

# Read the CSV file and extract wavelength and values for each channel
def read_csv(filename):
    data = {"RED": [], "GREEN": [], "BLUE": []}
    with open(filename, newline='') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            if len(row) == 6:
                # Check if the red channel has valid data
                if row[0] and row[1]:
                    data["RED"].append((float(row[0]), float(row[1])))
                # Check if the green channel has valid data
                if row[2] and row[3]:
                    data["GREEN"].append((float(row[2]), float(row[3])))
                # Check if the blue channel has valid data
                if row[4] and row[5]:
                    data["BLUE"].append((float(row[4]), float(row[5])))
    return data

# Update JSON structure with values and wavelength from CSV
def update_json(json_data, csv_data):
    for obj in json_data:
        channel = obj["channel"]
        values = csv_data[channel]
        obj["values"]["value"] = [v[1] for v in values]
        obj["wavelength"]["value"] = [v[0] for v in values]
    return json_data

# Example usage
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--manufacturer', type=str, help='Manufacturer name', required=True)
    parser.add_argument('--model', type=str, help='Model name', required=True)
    parser.add_argument('--dataSource', type=str, help='Data source', required=True)
    parser.add_argument('csv_filename', type=str, help='CSV filename')
    args = parser.parse_args()

    # Load JSON data
    json_data = [
        {
            "model": f"{args.manufacturer} {args.model}",
            "name": f"{args.manufacturer} {args.model} Red",
            "type": "OSC_SENSOR",
            "dataQualityMarker": 2,
            "dataSource": args.dataSource,
            "manufacturer": args.manufacturer,
            "channel": "RED",
            "comment": f"Covers all cameras using this chip",
            "version": 1,
            "wavelength": {"value": [], "units": "nm"},
            "values": {"value": [], "range": 1}
        },
        {
            "model": f"{args.manufacturer} {args.model}",
            "name": f"{args.manufacturer} {args.model} Green",
            "type": "OSC_SENSOR",
            "dataQualityMarker": 2,
            "dataSource": args.dataSource,
            "manufacturer": args.manufacturer,
            "channel": "GREEN",
            "comment": f"Covers all cameras using this chip",
            "version": 1,
            "wavelength": {"value": [], "units": "nm"},
            "values": {"value": [], "range": 1}
        },
        {
            "model": f"{args.manufacturer} {args.model}",
            "name": f"{args.manufacturer} {args.model} Blue",
            "type": "OSC_SENSOR",
            "dataQualityMarker": 2,
            "dataSource": args.dataSource,
            "manufacturer": args.manufacturer,
            "channel": "BLUE",
            "comment": f"Covers all cameras using this chip",
            "version": 1,
            "wavelength": {"value": [], "units": "nm"},
            "values": {"value": [], "range": 1}
        }
    ]

    # Read CSV data
    csv_data = read_csv(args.csv_filename)

    # Update JSON data
    updated_json = update_json(json_data, csv_data)

    # Print or save the updated JSON data
    print(json.dumps(updated_json, indent=4))

