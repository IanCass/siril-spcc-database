import json
import sys
from typing import Dict, Any, Optional

def remove_wavelength_duplicates(input_file: str, output_file: Optional[str] = None) -> Dict[str, Any]:
    """
    Removes duplicate wavelength values from a JSON file while keeping the corresponding values 
    synchronized and generates a report of the changes.
    
    Args:
        input_file (str): Path to the input JSON file
        output_file (str, optional): Path to save the modified JSON. If None, overwrites input file.
    
    Returns:
        Dict containing the report of duplicates removed
    """
    # Read the input JSON file
    try:
        with open(input_file, 'r') as f:
            data = json.load(f)
    except FileNotFoundError:
        print(f"Error: File {input_file} not found.")
        sys.exit(1)
    except json.JSONDecodeError:
        print(f"Error: Invalid JSON in file {input_file}.")
        sys.exit(1)
    
    # Initialize report dictionary
    report = {
        'total_entries_before': len(data[0]['wavelength']['value']),
        'duplicate_count': 0,
        'removed_wavelengths': []
    }
    
    # Create lists to store unique wavelengths and values
    unique_wavelengths = []
    unique_values = []
    seen_wavelengths = set()
    
    # Iterate through original wavelengths and values
    for wavelength, value in zip(data[0]['wavelength']['value'], data[0]['values']['value']):
        # If wavelength is not already seen, keep it
        if wavelength not in seen_wavelengths:
            unique_wavelengths.append(wavelength)
            unique_values.append(value)
            seen_wavelengths.add(wavelength)
        else:
            # Track duplicates
            report['duplicate_count'] += 1
            report['removed_wavelengths'].append(wavelength)
    
    # Update the data with unique wavelengths and values
    data[0]['wavelength']['value'] = unique_wavelengths
    data[0]['values']['value'] = unique_values
    
    # Complete the report
    report['total_entries_after'] = len(unique_wavelengths)
    
    # Determine output file path
    output_path = output_file or input_file
    
    # Write the modified data
    try:
        with open(output_path, 'w') as f:
            json.dump(data, f, indent=4)
    except IOError:
        print(f"Error: Unable to write to file {output_path}")
        sys.exit(1)
    
    # Print the report
    print("\nDuplication Removal Report:")
    print(f"Total entries before: {report['total_entries_before']}")
    print(f"Total entries after:  {report['total_entries_after']}")
    print(f"Duplicates removed:   {report['duplicate_count']}")
    if report['removed_wavelengths']:
        print("Removed wavelengths: ", report['removed_wavelengths'])
    
    # Return exit code 1 if duplicates were found
    if report['duplicate_count'] > 0:
        sys.exit(1)
        
    return report

# Main execution with command-line argument support
def main():
    if len(sys.argv) < 2:
        print("Usage: python remove_duplicates.py <input_file> [output_file]")
        sys.exit(1)
    
    input_file = sys.argv[1]
    output_file = sys.argv[2] if len(sys.argv) > 2 else None
    
    remove_wavelength_duplicates(input_file, output_file)

if __name__ == "__main__":
    main()
